local speed = 200
local gameover = false
local godmode = false
local time_init = os.time()
local playSec = 0
local playMin = 0

local right, left, down, up = 1, 2, 3, 4                                    -- direction flags


function love.load()
    -- set game speed from command line argument
    for iter, argument in pairs(arg) do
        if argument:find("--speed") then
            speed = tonumber(argument:sub(argument:find("=") + 1))
                or speed
        end
        if argument:find("-s") then
            speed = tonumber(arg[iter + 1])
                or speed
        end
        if argument:find("--god") or argument:find("-g") then
            godmode = true
        end
    end
    print("game speed: ", speed)

    -- set window properties
    love.window.setMode(0, 0)                                                -- fullscreen
    window = {
        w = love.graphics.getWidth(),
        h = love.graphics.getHeight(),
    }
    print("resolution: ", window.w, "x", window.h)

    -- set player and enemy properties
    player = {
        x = 100, s = (window.w/9 + window.h/9)/2,                            -- s = size, formula random
        y = 100,
    }
    enemy = {
        x = 300, dX = right, s = player.s/5,                                 -- d = direction, dX = direction X
        y = 300, dY = down,
    }
end



local pressed = love.keyboard.isDown                                         -- replace long, ugly function name

local function bump(A, B)
    return ((A.x + A.s > B.x) and (A.x < B.x + B.s))
       and ((A.y + A.s > B.y) and (A.y < B.y + B.s))
end

local function bounds(shape, direction)                                      -- returns true if shape can be moved at specified direction
    return direction == right and  shape.x <= window.w - shape.s
        or direction == left  and  shape.x >= 0
        or direction == down  and  shape.y <= window.h - shape.s
        or direction == up    and  shape.y >= 0
end


function love.update(dt)                                                     -- dt = time after previous love.update() call

            -- KEYBOARD EVENTS --

    if pressed("right") and bounds(player, right) then
        player.x = player.x + dt*speed
    end
    if pressed("left") and bounds(player, left) then
        player.x = player.x - dt*speed
    end
    if pressed("down") and bounds(player, down) then
        player.y = player.y + dt*speed
    end
    if pressed("up") and bounds(player, up) then
        player.y = player.y - dt*speed
    end

             -- ENEMY MOTION --

    if not godmode then
        local stepX = dt*speed/2 + math.random()*speed/6                    -- move enemy in pixels on X axis
        local stepY = dt*speed/2 + math.random()*speed/6                    -- move enemy in pixels on Y axis

        if enemy.dX == right then
            if bounds(enemy, right) then
                enemy.x = enemy.x + stepX
            else                                                            -- invert X direction
                enemy.x = enemy.x - stepX
                enemy.dX = left
            end
        else                                                                -- moved left
            if bounds(enemy, left) then
                enemy.x = enemy.x - stepX
            else                                                            -- invert X direction
                enemy.dX = right
                enemy.x = enemy.x + stepX
            end
        end

        if enemy.dY == down then
            if bounds(enemy, down) then
                enemy.y = enemy.y + stepY
            else                                                            -- invert Y direction
                enemy.y = enemy.y - stepY
                enemy.dY = up
            end
        else                                                                -- moved up
            if bounds(enemy, up) then
                enemy.y = enemy.y - stepY
            else                                                            -- invert Y direction
                enemy.y = enemy.y + stepY
                enemy.dY = down
            end
        end
    end

            -- GAMEOVER CHECK --

    if bump(player, enemy) or gameover then
        gameover = true
    else
        playSec = os.time() - time_init
        if playSec > 59 then
            playMin = playMin + 1
            time_init = os.time(); playSec = 0                              -- reset seconds
        end
    end
end



function love.draw()
    love.graphics.print(
        (playMin > 0) and playMin
                          .. ((playSec % 2 == 0) and ":" or " ")
                          .. playSec
                       or playSec)

    if not gameover then
        love.graphics.rectangle("fill", enemy.x, enemy.y, enemy.s, enemy.s)
        love.graphics.rectangle("fill", player.x, player.y, player.s, player.s)
    else
        love.graphics.print("Game Over", window.w/2 - 50, window.h/2 -10)
    end
end
